# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns
from django.conf.urls.defaults import url

urlpatterns = patterns("core.views",
    url(r"^$", "showMainPage"),
    url(r"^rejestracja/(\w{40})/$", "confirmRegistration"),
    url(r"^panel-uzytkownika/$", "showPanel"),
    url(r"^panel-uzytkownika/moje-dane/$", "changeUserData"),
    url(r"^panel-uzytkownika/dodaj-przepis/$", "addRecipe"),
    url(r"^panel-uzytkownika/ksiazka-kucharska/$", "showCookBook"),
    url(r"^zmien-haslo/$", "changeUserPassword"),
    url(r"^wylogowanie/$", "logoutPanel"),
    url(r"^przepisy-ajax/$", "returnRecipesForAjax"),
    url(r"^bing-token/$", "returnBingToken"),
    url(r"^szukaj/(?P<page>[\w\-_]+)/$", "showSearchResults", name = "search-results"),
    url(r"^(?P<subpage>[\w\-_]+)/$", "showSubpage", name = "subpage"),
    url(r"^(?P<slug>[\w\-_]+)\-(?P<pk>[\d]+)\.html$", "showRecipe"),
    url(r"^(?P<slug>[\w\-_]+)\-(?P<pk>[\d]+)\.html&action=(?P<action>[\w\-_]+)$", "runAction"),
    url(r"^captcha.png/(?P<url>[\w\-_]+)/$", "captchaGenerator", name = "captcha"),
)
