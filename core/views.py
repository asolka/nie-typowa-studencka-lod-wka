# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404

from django.db import IntegrityError

from django.template import RequestContext
from django.shortcuts import get_object_or_404

from django.shortcuts import render_to_response

from django.views.decorators.cache import never_cache

from django.contrib.auth import authenticate
from django.contrib.auth import login as loginPanel
from django.contrib.auth import logout

from config.models import Setup

from cms.models import Subpage

from cms.forms import RegisterForm
from cms.forms import LoginForm
from cms.forms import UserDataForm
from cms.forms import AddRecipe
from cms.forms import handle_uploaded_and_clear_file

from cms.utils import PasswordGenerator
from cms.utils import renderCapchaImage
from cms.utils import createPDF

from django.core.mail import EmailMessage

from core.models import RegisteredUser
from core.models import Recipe
from core.models import Category
from core.models import Ingredient
from core.models import Book
from core.models import Score

from hashlib import sha1

import os

import pycurl
import cStringIO

from django.core.paginator import Paginator
from django.core.paginator import InvalidPage
from django.core.paginator import EmptyPage

def showMainPage(request):
    return render_to_response("index.html", context_instance = RequestContext(request))

def returnRecipesForAjax(request):
    if request.is_ajax():
        return HttpResponse(",".join(["%s" % recipe.name for recipe in Recipe.objects.all()]))
    else:
        raise Http404

def returnBingToken(request):
    if request.is_ajax():
        response = cStringIO.StringIO()

        ClientID = ""
        ClientSecret = ""
        PostParams = "%s%s" % (ClientID, ClientSecret)

        c = pycurl.Curl()
        c.setopt(c.URL, "")
        c.setopt(c.WRITEFUNCTION, response.write)
        c.setopt(c.POSTFIELDS, PostParams)
        c.setopt(c.VERBOSE, True)
        c.perform()
        c.close()

        return HttpResponse(response.getvalue().split(":")[3].split(",")[0][1:-1])
    else:
        raise Http404

def showSubpage(request, subpage):
    subpage = get_object_or_404(Subpage, url = subpage)

    token_error = None

    if subpage.flag == 1:
        registerForm = RegisterForm()

        if request.POST:
            registerForm = RegisterForm(request.POST)

            if registerForm.is_valid():
                token_subpage_id = request.session.get("token_subpage_%s" % subpage.id, default = "")

                if sha1(registerForm.cleaned_data["token"]).hexdigest() == token_subpage_id:
                    flname = registerForm.cleaned_data["flname"]
                    email = registerForm.cleaned_data["email"]
                    nick = registerForm.cleaned_data["nick"]

                    flname = flname.split()

                    try:
                    	registered = RegisteredUser(fname = flname[0], lname = flname[-1], email = email, nick = nick)
	                registered.save()
    
        	        return HttpResponseRedirect("/zgloszenie-zostalo-przyjete/")
                    except IntegrityError:
                        return HttpResponseRedirect("/podany-adres-e-mail-znajduje-sie-juz-w-bazie-danych/")
                    except:
                        return HttpResponseRedirect("/wystapil-nieznany-blad/")
                else:
                    token_error = u"Nieprawidłowy token."

            data = request.POST.copy()
            data["token"] = u""
            setattr(registerForm, "data", data)

        if request.user.is_authenticated():
            return HttpResponseRedirect("/panel-uzytkownika/")

        return render_to_response("subpage.html", {"subpage" : subpage, "registerForm" : registerForm, "token_error" : token_error}, context_instance = RequestContext(request))

    elif subpage.flag == 2:
        loginForm = LoginForm()

        if request.POST:
            loginForm = LoginForm(request.POST)

            if loginForm.is_valid():
                login = request.POST.get("login")
                password = request.POST.get("password")

                try:
                    registered = RegisteredUser.objects.get(email = login)
    
                    if registered.confirmation:
                        user = authenticate(username = login, password = password)
    
                        if user and user.is_active:
                            loginPanel(request, user)
    
                            return HttpResponseRedirect("/panel-uzytkownika/")
                        else:
                            return HttpResponseRedirect("/podano-bledny-login-lub-haslo/")
                    else:
                        return HttpResponseRedirect("/brak-aktywacji-konta/")
                except:
                    return HttpResponseRedirect("/podano-bledny-login-lub-haslo/")

        if request.user.is_authenticated():
            return HttpResponseRedirect("/panel-uzytkownika/")

        return render_to_response("subpage.html", {"subpage" : subpage, "loginForm" : loginForm}, context_instance = RequestContext(request))

    return render_to_response("subpage.html", {"subpage" : subpage}, context_instance = RequestContext(request))

def showPanel(request):
    if request.user.is_authenticated():
        if request.user.is_superuser:
            return HttpResponseRedirect("/admin/")
    
        user = get_object_or_404(RegisteredUser, user_id = request.user.pk)

        return render_to_response("panel.html", {"user" : user}, context_instance = RequestContext(request))
    else:
        raise Http404

    return render_to_response("subpage.html", context_instance = RequestContext(request))

def confirmRegistration(request, activation_key):
    registered = get_object_or_404(RegisteredUser, activation_key = activation_key)

    if not registered.confirmation:
        registered.confirmation = True
        registered.save()

        return HttpResponseRedirect("/konto-zostalo-aktywowane/")
    else:
        return HttpResponseRedirect("/wygasniecie-linku-aktywacyjnego/")

def logoutPanel(request):
    logout(request)

    return HttpResponseRedirect("/uzytkownik-wylogowany-poprawnie/")

def changeUserData(request):
    if request.user.is_authenticated():
        userdataform = UserDataForm()
        user = get_object_or_404(RegisteredUser, user_id = request.user.pk)

        userdataform.initial["fname"] = user.fname
        userdataform.initial["lname"] = user.lname
        userdataform.initial["nick"] = user.nick

        if request.POST:
            userdataform = UserDataForm(request.POST)
    
            if userdataform.is_valid():
                fname = userdataform.cleaned_data["fname"]
                lname = userdataform.cleaned_data["lname"]
                nick = userdataform.cleaned_data["nick"]

                changes = 0

                if fname and user.fname != fname:
                    user.fname = fname
                    changes += 1
                if lname and user.lname != lname:
                    user.lname = lname
                    changes += 1
                if nick and user.nick != nick:
                    user.nick = nick
                    changes += 1

                if changes:
                    user.save()

                    return HttpResponseRedirect("/dane-zapisane-poprawnie/")
                else:
                    return render_to_response("panel.html", {"userdataform" : userdataform, "changes" : changes}, context_instance = RequestContext(request))

        return render_to_response("panel.html", {"userdataform" : userdataform}, context_instance = RequestContext(request))
    else:
        raise Http404

def changeUserPassword(request):
    if request.user.is_authenticated():
        user = get_object_or_404(RegisteredUser, user_id = request.user.pk)

        pw = PasswordGenerator(8)
        newPassword = pw.pswdGenerator()

        user.password = pw.pswdHash(password = newPassword)

        user.user.set_password(newPassword)
        user.user.save()

        user.save()

        title = u"e-kk.pl - Twoje hasło zostało zresetowane"
        text = u"Witaj,<br><br>Z tej strony kłania się administrator z <a href=\"http://e-kk.pl\" title=\"Internetowa książka kucharska\">e-kk.pl</a>.<br><br>Właśnie <strong>zresetowaliśmy</strong> hasło do Twojego konta.<br><br>Nowe hasło: <strong>%s</strong><br><br>Z poważaniem,<br>Administrator<br><a href=\"mailto:support@e-kk.pl\" title=\"support@e-kk.pl\">support@e-kk.pl</a>" % newPassword
        who = u"e-kk.pl <support@e-kk.pl>"

        msg = EmailMessage(title, text, who, [user.email])
        msg.content_subtype = "html"
        msg.send()

        logout(request)

        return HttpResponseRedirect("/haslo-zostalo-zresetowane/")
    else:
        raise Http404

def addRecipe(request):
    if request.user.is_authenticated():
        addRecipe = AddRecipe()
    
        if request.POST:
            addRecipe = AddRecipe(request.POST, request.FILES)
    
            if addRecipe.is_valid():
                name = addRecipe.cleaned_data["name"]
                category = addRecipe.cleaned_data["category"]
                text = addRecipe.cleaned_data["text"]
                products = addRecipe.cleaned_data["products"]
                vegan = addRecipe.cleaned_data["vegan"]
                jude = addRecipe.cleaned_data["jude"]

                photo = handle_uploaded_and_clear_file(request.FILES["photo"])

                user = get_object_or_404(RegisteredUser, user_id = request.user.pk)

                recipe = Recipe(author = user, parent = Category.objects.get(pk = category), name = name, description = text, vegan = vegan, jude = jude, photo = photo)
                recipe.save()

                for product in products.split(","):
                    try:
                        ingredient = Ingredient(name = product.strip())
                        ingredient.save()
                    except IntegrityError:
                        recipe.products.add(Ingredient.objects.get(name = product.strip()))
                    else:
                        recipe.products.add(ingredient)

                return HttpResponseRedirect("/przepis-dodany-poprawnie/")

        return render_to_response("panel.html", {"addRecipe" : addRecipe}, context_instance = RequestContext(request))
    else:
        raise Http404

def showRecipe(request, slug, pk):
    recipe = Recipe.objects.get(url = u"%s-%s.html" % (slug, pk))

    cookbook = None
    vote = None

    try:
        user = get_object_or_404(RegisteredUser, user_id = request.user.pk)

        vote = Score.objects.filter(user_id = user.pk).filter(recipe_id = pk)[0]

        try:
            cookbook = Book.objects.filter(owner = user).filter(storage = recipe)[0]
        except:
            pass
    except:
        pass

    return render_to_response("recipe.html", {"recipe" : recipe, "score" : recipe.p_vote + recipe.m_vote, "vote" : vote, "cookbook" : cookbook}, context_instance = RequestContext(request))

def runAction(request, slug, pk, action):
    if request.user.is_authenticated():
        user = get_object_or_404(RegisteredUser, user_id = request.user.pk)
        recipe = Recipe.objects.get(url = u"%s-%s.html" % (slug, pk))
        
        if action == "cookbook":
            book = Book.objects.get_or_create(owner = user)
            book[0].storage.add(recipe)

            return HttpResponseRedirect("/ksiazka-kucharska-zaktualizowana-poprawnie/")
        elif action == "pdf":
            path = createPDF(recipe)

            response = HttpResponse(open(path).read(), mimetype = "application/force-download")
            response["Content-Disposition"] = "attachment; filename=%s" % os.path.basename(path)
            response["X-Sendfile"] = ":)"

            return response
        try:
            vote = Score.objects.filter(user_id = user.pk).filter(recipe_id = pk)[0]
        except:
            vote = None

        if not vote:
            if action == "plus":
                recipe.p_vote += 1
                recipe.save()
        
            elif action == "minus":
                recipe.m_vote += 1
                recipe.save()
    
            score = Score(user = user, recipe = recipe)
            score.save()
        
            return HttpResponseRedirect("/przepis-oceniony-poprawnie/")
        else:
            raise Http404
    else:
        raise Http404

def showCookBook(request):
    if request.user.is_authenticated():
        user = get_object_or_404(RegisteredUser, user_id = request.user.pk)
        
        try:
            book = Book.objects.filter(owner = user)[0]
        except:
            book = None

        return render_to_response("panel.html", {"book" : book, "cookbook" : True}, context_instance = RequestContext(request))
    else:
        raise Http404

def showSearchResults(request, page = "1"):
    phrase = request.GET.get("fraza", u"").strip()

    count = 0

    if phrase:
        recipes = Recipe.objects.filter(name__contains = phrase)

        paginator = Paginator(recipes, Setup.objects.get_or_create(id = 1)[0].page_paginator)

        try:
            results = paginator.page(int(page))

            count = len(recipes)

        except (EmptyPage, InvalidPage, ValueError):
            return HttpResponseRedirect("/szukaj/1/?fraza=")
    else:
        results = []

    return render_to_response("search.html", {"results" : results, "phrase" : phrase, "count" : count}, context_instance = RequestContext(request))

def error500(request):
    return render_to_response("subpage.html", {"error" : "500"}, context_instance = RequestContext(request))

def error404(request):
    return render_to_response("subpage.html", {"error" : "404"}, context_instance = RequestContext(request))

@never_cache
def captchaGenerator(request, url):
    setup = Setup.objects.get_or_create(id = 1)[0]
    captcha = renderCapchaImage(setup.captcha_width, setup.captcha_height, setup.captcha_length, setup.captcha_font_size)

    response = HttpResponse()
    response["Content-Type"] = "image/png"
    response.write(captcha["picture"])

    request.session["token_%s" % url] = captcha["imghash"]

    return response