# -*- coding: utf-8 -*-

from django.contrib import admin

from core.models import RegisteredUser

from core.models import Category
from core.models import Ingredient
from core.models import Recipe
from core.models import Book
from core.models import Score

class RegisteredUser_Admin(admin.ModelAdmin):
    list_per_page = 50
    list_display = ("email", "fname", "lname", "nick", "date", "confirmation")

    search_fields = ("email", "lname", "nick")

    list_filter = ["confirmation"]

    class Media:
        js = ["/statics/js/jquery-1.9.1.min.js", "/statics/js/input-hider.js"]

class Category_Admin(admin.ModelAdmin):
    list_display = ("name",)

    class Media:
        js = ["/statics/js/jquery-1.9.1.min.js", "/statics/js/input-hider.js"]

class Ingredient_Admin(admin.ModelAdmin):
    list_display = ("name",)

    ordering = ("name",)

    class Media:
        js = ["/statics/js/jquery-1.9.1.min.js", "/statics/js/input-hider.js"]

class Recipe_Admin(admin.ModelAdmin):
    list_display = ("author", "parent", "name", "points")

    list_filter = ["author", "parent__name"]

    search_fields = ("name", "author__email")

    class Media:
        js = ["/statics/js/jquery-1.9.1.min.js", "/statics/js/input-hider.js"]

class Book_Admin(admin.ModelAdmin):
    list_display = ("owner",)

    search_fields = ("owner__email",)

    class Media:
        js = ["/statics/js/jquery-1.9.1.min.js", "/statics/js/input-hider.js"]

class Score_Admin(admin.ModelAdmin):
    list_display = ("user", "recipe")

    list_filter = ["user"]

    def has_add_permission(self, request, obj = None):
        return False

admin.site.register(RegisteredUser, RegisteredUser_Admin)
admin.site.register(Category, Category_Admin)
admin.site.register(Ingredient, Ingredient_Admin)
admin.site.register(Recipe, Recipe_Admin)
admin.site.register(Book, Book_Admin)
admin.site.register(Score, Score_Admin)