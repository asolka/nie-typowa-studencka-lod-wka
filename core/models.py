# -*- coding: utf-8 -*-

from django.db import models

from django.contrib.auth.models import User
from django.contrib.auth.models import Group

from django.core.mail import EmailMessage

import string
from random import choice

from cms.utils import PasswordGenerator
from cms.utils import _slugify

class Category(models.Model):
    name = models.CharField(u"Nazwa", max_length = 30)

    class Meta:
        verbose_name = u"kategorię"
        verbose_name_plural = u"kategorie"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class Ingredient(models.Model):
    name = models.CharField(u"Nazwa", max_length = 30, unique = True)   

    class Meta:
        verbose_name = u"składnik"
        verbose_name_plural = u"składniki"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class RegisteredUser(models.Model):
    user = models.OneToOneField(User, editable = False, null = True, blank = True)
    activation_key = models.CharField(max_length = 40, editable = False)
    password = models.CharField(u"Hasło", max_length = 54, editable = False)
    date = models.DateTimeField(u"Data rejestracji", auto_now_add = True) 
    fname = models.CharField(u"Imię", max_length = 20)
    lname = models.CharField(u"Nazwisko", max_length = 30)
    nick = models.CharField(u"Nick", max_length = 12, blank = True, null = True)
    email = models.EmailField(u"E-mail", unique = True)
    confirmation = models.BooleanField(u"Stan konta (aktywne)", default = False)

    class Meta:
        verbose_name = u"użytkownika"
        verbose_name_plural = u"użytkownicy"

    def save(self, *args, **kwargs):
        size = 40

        if not self.pk:
            super(RegisteredUser, self).save(*args, **kwargs)

        if not self.password:
            self.pin = unicode(self.pk).zfill(4)
            self.activation_key = "".join([choice(string.letters + string.digits) for x in xrange(size)])

            pw = PasswordGenerator(8)
            self.newPassword = pw.pswdGenerator()
            self.password = pw.pswdHash(password = self.newPassword)

            self.user = User.objects.create_user(self.email, self.email, self.newPassword)
            self.user.is_staff = False
            self.user.save()

            group = Group.objects.get(name = u"Użytkownicy")
            group.user_set.add(self.user)

            title = u"e-kk.pl - Twoje konto zostało utworzone"
            text = u"Witaj,<br><br>Z tej strony kłania się administrator z <a href=\"http://e-kk.pl\" title=\"Internetowa książka kucharska\">e-kk.pl</a>.<br><br>Widzię, że <strong>utworzyłeś</strong> konto w naszym serwisie. Chcemy Cię prosić o jego potwierdzenie.<br><br>W tym celu wystarczy kliknąć ten <a href=\"http://e-kk.pl/rejestracja/%s/\" title=\"Link aktywacyjny\">link aktywacyjny</a>. Wówczas uzyskasz dostęp do panelu użytkownika i szerszej oferty naszego serwisu.<br><br>Twój login i hasło to:<br><br><strong>Login:</strong> %s<br><strong>Hasło:</strong> %s<br><br>Jeśli nie rejestrowałeś u nas konta, to proszę zignoruj tę wiadomość.<br><br>Z poważaniem,<br>Administrator<br><a href=\"mailto:support@e-kk.pl\" title=\"support@e-kk.pl\">support@e-kk.pl</a>" % (self.activation_key, self.email, self.newPassword)
            who = u"e-kk.pl <support@e-kk.pl>"

            msg = EmailMessage(title, text, who, [self.email])
            msg.content_subtype = "html"
            msg.send()

        if self.pk:
            user = User.objects.get(pk = self.user_id)
    
            if user.email != self.email:
                user.username = self.email
                user.email = self.email
                user.save()

        super(RegisteredUser, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.user.delete()

        title = u"e-kk.pl - Twoje konto zostało usunięte"
        text = u"Witaj,<br><br>Z tej strony kłania się administrator z <a href=\"http://e-kk.pl\" title=\"Internetowa książka kucharska\">e-kk.pl</a>.<br><br>Twoje konto zostało właśnie <strong>usunięte</strong>.<br><br>Z poważaniem,<br>Administrator<br><a href=\"mailto:support@e-kk.pl\" title=\"support@e-kk.pl\">support@e-kk.pl</a>"
        who = u"e-kk.pl <support@e-kk.pl>"

        msg = EmailMessage(title, text, who, [self.email])
        msg.content_subtype = "html"
        msg.send()

        super(RegisteredUser, self).delete(*args, **kwargs)

    def __str__(self):
        return self.email

    def __unicode__(self):
        return self.email

class Recipe(models.Model):
    author = models.ForeignKey(RegisteredUser, verbose_name = u"Autor", limit_choices_to = {"confirmation" : True})
    parent = models.ForeignKey(Category, verbose_name = u"Kategoria")
    name = models.CharField(u"Tytuł", max_length = 50)
    description = models.TextField(u"Opis przyrządzania")
    products = models.ManyToManyField(Ingredient, verbose_name = u"Składniki")
    products.help_text = ""
    vegan = models.BooleanField(u"Danie wegetariańskie", default = False)
    jude = models.BooleanField(u"Danie koszerne", default = False)
    photo = models.ImageField(verbose_name = u"Zdjęcie", upload_to = "photos", blank = True, null = True)
    p_vote = models.PositiveIntegerField(editable = False, default = 0)
    m_vote = models.PositiveIntegerField(editable = False, default = 0)
    date = models.DateTimeField(u"Data rejestracji", auto_now_add = True)
    url = models.SlugField(verbose_name = u"Adres url", editable = False, max_length = 200)

    def points(self):
        return u"%d/%d" % (self.p_vote, self.m_vote)

    points.allow_tags = True
    points.short_description = u"Punktacja"

    class Meta:
        verbose_name = u"przepisu"
        verbose_name_plural = u"przepisy"

    def save(self, *args, **kwargs):
        if not self.pk:
            super(Recipe, self).save(*args, **kwargs)

        self.url = u"%s-%s.html" % (_slugify(self.name), self.pk)

        self.description = self.description.replace("\r\n", "<br>")

        super(Recipe, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return u"/%s" % self.url

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class Score(models.Model):
    user = models.ForeignKey(RegisteredUser, verbose_name = u"Autor")
    recipe = models.ForeignKey(Recipe, verbose_name = u"Przepis")

    class Meta:
        verbose_name = u"oddany głos"
        verbose_name_plural = u"oddane głosy"

    def __str__(self):
        return self.user.email

    def __unicode__(self):
        return self.user.email

class Book(models.Model):
    owner = models.ForeignKey(RegisteredUser, verbose_name = u"Autor", limit_choices_to = {"confirmation" : True})
    storage = models.ManyToManyField(Recipe, verbose_name = u"Przepisy")

    class Meta:
        verbose_name = u"książkę kucharską"
        verbose_name_plural = u"książka kucharska"

    def __str__(self):
        return self.owner.email

    def __unicode__(self):
        return self.owner.email