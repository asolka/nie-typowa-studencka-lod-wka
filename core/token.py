import pycurl
import cStringIO

response = cStringIO.StringIO()

ClientID = ""
ClientSecret = ""
PostParams = "%s%s" % (ClientID, ClientSecret)

c = pycurl.Curl()

c.setopt(c.URL, "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13")
c.setopt(c.WRITEFUNCTION, response.write)
c.setopt(c.POSTFIELDS, PostParams)
c.setopt(c.VERBOSE, True)

c.perform()
c.close()