# -*- coding: utf-8 -*-

from django.db import models

class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

class Setup(SingletonModel):
    title = models.CharField(u"Tytuł", max_length = 70, blank = True, null = True)
    description = models.CharField(u"Opis", max_length = 160, blank = True, null = True)
    keywords = models.CharField(u"Słowa kluczowe", max_length = 130, blank = True, null = True)

    page_paginator = models.PositiveSmallIntegerField(u"Paginacja", default = 5)
    truncatewords = models.PositiveSmallIntegerField(u"Obcinanie treści", default = 100)

    captcha_height = models.PositiveSmallIntegerField(u"Wysokość", default = 30)
    captcha_width = models.PositiveSmallIntegerField(u"Szerokość", default = 120)
    captcha_length = models.PositiveSmallIntegerField(u"Ilość znaków", default = 8)
    captcha_font_size = models.PositiveSmallIntegerField(u"Rozmiar czcionki", default = 18)

    class Meta:
        verbose_name = u"ustawienia"
        verbose_name_plural = u"ustawienia"