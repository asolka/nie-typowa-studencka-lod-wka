# -*- coding: utf-8 -*-

try:
    import Image
except ImportError:
    from PIL import Image

from django.db import models

from cms.utils import _slugify

class Subpage(models.Model):
    title = models.CharField(u"Tytuł", max_length = 150, unique = True)
    description = models.CharField(u"Opis", max_length = 160, blank = True, null = True)
    keywords = models.CharField(u"Słowa kluczowe", max_length = 130, blank = True, null = True)
    url = models.SlugField(max_length = 150, editable = False)
    text = models.TextField(u"Treść", blank = True, null = True)
    flags = ((-1, u"Standardowa"), (0, u"Formularz kontaktowy"), (1, u"Formularz rejestracyjny"), (2, u"Formularz logowania"))
    flag = models.IntegerField(u"Flaga", max_length = 150, choices = flags, default = -1)
    availability = models.BooleanField(u"Dostępność w sitemaps", default = True)
    date = models.DateTimeField(u"Data ostatniej edycji", auto_now = True)

    def chosenFlag(self):
        return self.flags[self.flag + 1][1]

    chosenFlag.allow_tags = True
    chosenFlag.short_description = u"Flaga"

    class Meta:
        verbose_name = u"podstronę"
        verbose_name_plural = u"podstrony"

    def save(self, *args, **kwargs):
        self.url = _slugify(self.title)

        super(Subpage, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        return (u"subpage", (), {"subpage": self.url})

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.title)

class Partner(models.Model):
    title = models.CharField(verbose_name = u"Tytuł", max_length = 100, unique = True)
    href = models.URLField(verbose_name = u"Hiperłącze")
    logo = models.ImageField(verbose_name = u"Logo", upload_to = "partners")
    width = models.PositiveSmallIntegerField(editable = False, default = 0)
    height = models.PositiveSmallIntegerField(editable = False, default = 0)

    def save(self, *args, **kwargs):
        if self.pk:
            this = Partner.objects.get(pk = self.pk)

            if this.logo != self.logo:
                this.logo.delete(save = False)

            super(Partner, self).save(*args, **kwargs)

        if not self.pk:
            super(Partner, self).save(*args, **kwargs)

        self.width, self.height = Image.open(open(self.logo.path)).size

        super(Partner, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        storage, path = self.logo.storage, self.logo.path

        super(Partner, self).delete()

        storage.delete(path)

    class Meta:
        verbose_name = u"partnera"
        verbose_name_plural = u"partnerzy"

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.title)