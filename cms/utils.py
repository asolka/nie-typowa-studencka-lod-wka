# -*- coding: utf-8 -*-

from PIL import Image
import ImageDraw
import ImageFont

import cStringIO
import hashlib
import string
import random

from hashlib import sha1

from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate
from reportlab.platypus import Paragraph
from reportlab.platypus import Spacer
from reportlab.platypus import Image as Im
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics

from django.conf import settings

def renderCapchaImage(width, height, length, fontSize):
    imgtext = "".join([random.choice(string.ascii_letters + string.digits + "-+!@%?&#$=") for x in xrange(length)])
    imghash = hashlib.sha1(imgtext).hexdigest()
    img = Image.new("RGB", (width, height), "#FFFFFF")
    draw = ImageDraw.Draw(img)

    r, g, b = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
    dr = (random.randint(0, 255) - r) / 300
    dg = (random.randint(0, 255) - g) / 300
    db = (random.randint(0, 255) - b) / 300

    for x in xrange(300):
        r, g, b = r + dr, g + dg, b + db
        draw.line((x, 0, x, 300), fill = (r, g, b))

    font = ImageFont.truetype(settings.STATIC_FONT, fontSize)
    parm = draw.textsize(imgtext, font)
    draw.text(((width - parm[0]) * 0.5, (height - parm[1]) * 0.5), imgtext, font = font, fill = (255, 255, 255))

    captchaFile = cStringIO.StringIO()
    img.save(captchaFile, "PNG")
    captchaFile.seek(0)
    data = {"imghash" : imghash, "picture" : captchaFile.read()}

    return data

class PasswordGenerator(object):
    def __init__(self, length, chars = string.ascii_lowercase + string.ascii_uppercase + string.digits):
        self.length = length
        self.chars = chars

    def saltGenerator(self):
        return u"".join(random.choice(self.chars) for x in xrange(self.length))

    def pswdGenerator(self):
        return self.saltGenerator()

    def pswdHash(self, engine = "sha1", password = None):
        salt = self.saltGenerator()
        password = sha1(salt + password).hexdigest()

        return u"%s$%s$%s" % (engine, salt, password)

    def pswdIsValid(self, enterPswd, validPswd):
        engine, salt, password = validPswd.split(u"$")
        enterPswd = sha1(salt + enterPswd).hexdigest()

        return password == enterPswd

def createPDF(recipe):
    path = "%s/pdf/e-kk.pl-%s.pdf" % (settings.MEDIA_ROOT, _slugify(recipe.name))
    pdfmetrics.registerFont(TTFont("Istok-Web-Regular", settings.STATIC_FONT))

    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name = "Justify", fontName = "Istok-Web-Regular", fontSize = 12, alignment = TA_JUSTIFY))
    styles.add(ParagraphStyle(name = "Footer", fontName = "Istok-Web-Regular", fontSize = 10, alignment = 0))

    doc = SimpleDocTemplate(path, pagesize = letter, rightMargin = 72, leftMargin = 72, topMargin = 72, bottomMargin = 18)

    Story = []

    if recipe.photo:
        im = Im("%s%s" % (settings.ROOT_DIR, recipe.photo), 3 * inch, 3 * inch)

    Story.append(Paragraph(u"<font size=25>e-kk.pl - (nie)Typowa studencka lodówka</font>", styles["Justify"]))
    Story.append(Spacer(1, 30))

    Story.append(Paragraph(u"%s" % recipe.name, styles["Justify"]))
    Story.append(Spacer(1, 20))

    Story.append(Paragraph(u"%s" % recipe.description.replace("<br>", "<br/>"), styles["Justify"]))
    Story.append(Spacer(1, 20))

    if recipe.photo:
        Story.append(im)
        Story.append(Spacer(1, 20))

    Story.append(Paragraph(u"<u>Produkty:</u>%s" % "".join(["<br/>- %s" % product.name for product in recipe.products.all()]), styles["Justify"]))
    Story.append(Spacer(1, 20))

    if recipe.vegan:
        Story.append(Paragraph(u"<u>Informacje dodatkowe:</u><br/>Jest to danie wegetariańskie.", styles["Justify"]))
    elif recipe.jude:
        Story.append(Paragraph(u"<u>Informacje dodatkowe:</u><br/>Jest to danie koszerne.", styles["Justify"]))

    Story.append(Spacer(1, 10))
    Story.append(Paragraph(u"<u>Adres url:</u><br/><a href='%s%s'>%s%s</a>" % (settings.DOMAIN_NAME, recipe.url, settings.DOMAIN_NAME, recipe.url), styles["Justify"]))

    doc.build(Story)

    return path

from django.template.defaultfilters import slugify

def _slugify(text):
    return slugify(text.replace(u"ł", u"l").replace(u"Ł", u"L"))