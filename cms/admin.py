# -*- coding: utf-8 -*-

from django.contrib import admin

from cms.models import Subpage
from cms.models import Partner

class Subpage_Admin(admin.ModelAdmin):
    list_display = ("title", "chosenFlag", "availability")
    list_filter = ["flag", "availability"]

    search_fields = ("title",)

    class Media:
        js = ["/statics/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js", "/statics/js/tinymce_setup.js"]

class Partner_Admin(admin.ModelAdmin):
    list_display = ("title", "href")

admin.site.register(Subpage, Subpage_Admin)
admin.site.register(Partner, Partner_Admin)