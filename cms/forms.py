# -*- coding: utf-8 -*-

from django import forms

from core.models import Category

from django.conf import settings

import os

def handle_uploaded_and_clear_file(f, clear = False):
    path = "%s/recipes/" % (settings.MEDIA_ROOT)

    if not clear:
        if f.size > 2621440:
            return u"Przekroczono maksymalny rozmiar pliku (2.5 MB)."

        if os.path.splitext(f.name)[1] not in (".png", ".gif", ".jpg", ".jpeg"):
            return u"Niedozwolony typ pliku. Załącznik obsługuje wyłącznie: png, gif, jpg i jpeg."

        if not os.path.exists(path):
            os.mkdir(path)

        destination = open("%s%s" % (path, f.name), "wb+")

        for chunk in f.chunks():
            destination.write(chunk)

        destination.close()

        return "/media/recipes/%s" % f.name
    else:
        os.remove("%s/%s" % (path, f))

class RegisterForm(forms.Form):
    flname = forms.CharField(max_length = 50, widget = forms.TextInput(attrs = {"title" : u"Wpisz imię i nazwisko"}))
    email = forms.EmailField(widget = forms.TextInput(attrs = {"title" : u"Wpisz adres e-mail"}))
    nick = forms.CharField(max_length = 12, widget = forms.TextInput(attrs = {"title" : u"Wpisz nick"}), required = False)
    token = forms.CharField(max_length = 8, widget = forms.TextInput(attrs = {"title" : u"Przepisz token z obrazka", "id" : "captcha", "placeholder" : "Przepisz token z obrazka"}))
    regulations = forms.BooleanField(widget = forms.CheckboxInput(attrs = {"title" : u"Zaakceptuj regulamin", "id" : "chk"}), required = True, initial = False)

class LoginForm(forms.Form):
    login = forms.EmailField(widget = forms.TextInput(attrs = {"title" : u"Wpisz adres e-mail"}))
    password = forms.CharField(max_length = 8, widget = forms.PasswordInput(attrs = {"title" : u"Wpisz hasło"}))

class UserDataForm(forms.Form):
    fname = forms.CharField(max_length = 12, widget = forms.TextInput(attrs = {"title" : u"Wpisz imię",}))
    lname = forms.CharField(max_length = 20, widget = forms.TextInput(attrs = {"title" : u"Wpisz nazwisko"}))
    nick = forms.CharField(max_length = 12, widget = forms.TextInput(attrs = {"title" : u"Wpisz nick"}), required = False)

class AddRecipe(forms.Form):
    name = forms.CharField(max_length = 50, widget = forms.TextInput(attrs = {"title" : u"Wpisz tytuł"}))
    category = forms.ChoiceField(choices = [(category.id, category.name) for category in Category.objects.all()])
    text = forms.CharField(widget = forms.Textarea(attrs = {"title" : u"Wpisz opis"}))
    products = forms.CharField(widget = forms.Textarea(attrs = {"title" : u"Wpisz składniki"}))
    vegan = forms.BooleanField(widget = forms.CheckboxInput(attrs = {"title" : u"Danie wegetariańskie", "style" : "margin-top: 10px; display: inline-block;height: 13px; width: 20px;"}), required = False, initial = False)
    jude = forms.BooleanField(widget = forms.CheckboxInput(attrs = {"title" : u"Danie koszerne", "style" : "margin: 10px 0 0 30px; display: inline-block;height: 13px; width: 20px;"}), required = False, initial = False)
    photo = forms.FileField(required = False)