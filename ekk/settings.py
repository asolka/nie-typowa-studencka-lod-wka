DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    "default": {
        "ENGINE": "",
       	"NAME": "",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}

TIME_ZONE = ""

LANGUAGE_CODE = ""

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

DIR = ""

MEDIA_ROOT = "%smedia" % DIR

MEDIA_URL = "/media/"

STATIC_ROOT = "%sstatics" % DIR

STATIC_URL = "/static/"

STATIC_FONT = "%s/fonts/Istok-Web-Regular.ttf" % STATIC_ROOT

STATIC_TEMPLATES = "%sstatics/" % DIR

STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

SECRET_KEY = "uv3z=lk+l7n!bip_*zcm4ew^*8h&amp;#v=*_+fii%-o12l_8um4rw"

TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.auth.context_processors.auth",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "ekk.processors.showDefaultMeta",
    "ekk.processors.showTopRecipes",
    "ekk.processors.showPartners",
)

MIDDLEWARE_CLASSES = (
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.gzip.GZipMiddleware",
)

ROOT_URLCONF = "ekk.urls"

WSGI_APPLICATION = "ekk.wsgi.application"

TEMPLATE_DIRS = ("%score/templates" % DIR,)

INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sitemaps",
    "grappelli",
    "django.contrib.admin",
    "config",
    "core",
    "cms"
)