# -*- coding: utf-8 -*-

from config.models import Setup

from core.models import Recipe
from cms.models import Partner

def showDefaultMeta(request):
    config = Setup.objects.get_or_create(id = 1)

    return {"config" : config[0]}

def showTopRecipes(request):
    return {"top_recipes" : sorted([(recipe.name, recipe.p_vote - recipe.m_vote, recipe.url) for recipe in Recipe.objects.all()], key = lambda i: i[1], reverse = True)[:5]}

def showPartners(request):
    return {"partners" : Partner.objects.all()}