# -*- coding: utf-8 -*-

from django.conf.urls import patterns
from django.conf.urls import url

from django.contrib.sitemaps import GenericSitemap

from cms.models import Subpage
from core.models import Recipe

info_dict_subpage = {
    "queryset": Subpage.objects.filter(availability = True),
    "date_field": "date",
}

info_dict_recipe = {
    "queryset": Recipe.objects.all().order_by("-date"),
    "date_field": "date",
}

sitemaps_subpage = {
    "subpage": GenericSitemap(info_dict_subpage, priority = 0.6, changefreq = "daily"),
}

sitemaps_recipe = {
    "recipe": GenericSitemap(info_dict_recipe, priority = 0.8, changefreq = "daily"),
}

urlpatterns = patterns("",  
    url(r"^\.xml$", "django.contrib.sitemaps.views.sitemap", {"sitemaps": sitemaps_subpage}),
    url(r"^/recipes.xml$", "django.contrib.sitemaps.views.sitemap", {"sitemaps": sitemaps_recipe})
)
